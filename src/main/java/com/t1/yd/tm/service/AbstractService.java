package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IRepository;
import com.t1.yd.tm.api.service.IService;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import com.t1.yd.tm.model.AbstractEntity;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntity, R extends IRepository<E>> implements IService<E> {

    @NotNull
    protected final R repository;

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    public E add(@NotNull final E entity) {
        return repository.add(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public E findOneById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public E findOneByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id) != null;
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public @NotNull Collection<E> set(@NotNull Collection<E> collection) {
        return repository.set(collection);
    }

    @Override
    public @NotNull Collection<E> add(@NotNull Collection<E> collection) {
        return repository.add(collection);
    }
}