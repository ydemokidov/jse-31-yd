package com.t1.yd.tm.client;

import com.t1.yd.tm.api.endpoint.ISystemEndpoint;
import com.t1.yd.tm.dto.request.ServerAboutRequest;
import com.t1.yd.tm.dto.request.ServerVersionRequest;
import com.t1.yd.tm.dto.response.ServerAboutResponse;
import com.t1.yd.tm.dto.response.ServerVersionResponse;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient systemEndpointClient = new SystemEndpointClient();
        systemEndpointClient.connect();

        final ServerAboutResponse serverAboutResponse = systemEndpointClient.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        final ServerVersionResponse serverVersionResponse = systemEndpointClient.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        systemEndpointClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

}
