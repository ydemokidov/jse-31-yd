package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.ServerAboutRequest;
import com.t1.yd.tm.dto.request.ServerVersionRequest;
import com.t1.yd.tm.dto.response.ServerAboutResponse;
import com.t1.yd.tm.dto.response.ServerVersionResponse;
import org.jetbrains.annotations.NotNull;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
