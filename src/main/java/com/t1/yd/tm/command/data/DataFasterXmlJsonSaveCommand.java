package com.t1.yd.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlJsonSaveCommand extends AbstractFasterXmlSaveCommand {

    @NotNull
    private final String name = "save_fasterxml_json";

    @NotNull
    private final String description = "Save json with FasterXML library";

    @Override
    @NotNull
    protected ObjectMapper getMapper() {
        return new ObjectMapper();
    }

    @Override
    @NotNull
    protected String getFile() {
        return FILE_JSON;
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
