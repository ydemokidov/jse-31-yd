package com.t1.yd.tm.component;

import com.t1.yd.tm.dto.request.AbstractRequest;
import com.t1.yd.tm.dto.response.AbstractResponse;
import com.t1.yd.tm.endpoint.Operation;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(@NotNull final Class<RQ> reqClass, @NotNull final Operation<RQ, RS> operation) {
        map.put(reqClass, operation);
    }

    public Object call(@NotNull final AbstractRequest request) {
        final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
