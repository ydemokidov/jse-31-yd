package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.api.endpoint.ISystemEndpoint;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.IServiceLocator;
import com.t1.yd.tm.dto.request.ServerAboutRequest;
import com.t1.yd.tm.dto.request.ServerVersionRequest;
import com.t1.yd.tm.dto.response.ServerAboutResponse;
import com.t1.yd.tm.dto.response.ServerVersionResponse;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
