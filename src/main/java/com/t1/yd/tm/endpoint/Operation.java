package com.t1.yd.tm.endpoint;

import com.t1.yd.tm.dto.request.AbstractRequest;
import com.t1.yd.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
